#include "textplugin.h"
#include <QInputDialog>


QString TextPlugin::name() const
{
    return QString("Insert custom Text");
}

QPainterPath TextPlugin::getObject(QWidget *parent)
{
    QPainterPath ppath;
    QString text = QInputDialog::getText(parent, QString("Insert Text"), QString("Text"));
    if(!text.isEmpty()) {
        QString msg = "Text = "+text;
        qInfo( msg.toStdString().c_str() );
        ppath.addText(0, 80, QFont("Cambira", 60), text);
    }
    return ppath;
}
