#-------------------------------------------------
#
# Project created by QtCreator 2017-05-08T08:28:24
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = TextPlugin
TEMPLATE = lib
CONFIG += plugin static

DESTDIR = ../plugins

SOURCES += textplugin.cpp
INCLUDEPATH += ../main

HEADERS += textplugin.h
DISTFILES += TextPlugin.json

unix {
    target.path = /usr/lib
    INSTALLS += target
}
