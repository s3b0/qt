#ifndef TEXTPLUGIN_H
#define TEXTPLUGIN_H

#include <QGenericPlugin>
#include "interface.h"

class TextPlugin : public QObject, public InsertInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.PainterDemo.InsertInterface" FILE "TextPlugin.json")
    Q_INTERFACES(InsertInterface)
public:
    QString name() const;
    QPainterPath getObject(QWidget *parent);
};

#endif // TEXTPLUGIN_H
