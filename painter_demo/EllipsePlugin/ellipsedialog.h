#ifndef ELLIPSEDIALOG_H
#define ELLIPSEDIALOG_H

#include <QDialog>

namespace Ui {
class EllipseDialog;
}

class EllipseDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EllipseDialog(QWidget *parent = 0);
    ~EllipseDialog();

private:
    Ui::EllipseDialog *ui;

signals:
    void accepted(qreal, qreal, qreal, qreal);
private slots:
    void onAccepted();
};

#endif // ELLIPSEDIALOG_H
