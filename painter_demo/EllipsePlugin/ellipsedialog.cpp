#include "ellipsedialog.h"
#include "ui_ellipsedialog.h"

EllipseDialog::EllipseDialog(QWidget *parent) : QDialog(parent), ui(new Ui::EllipseDialog)
{
    ui->setupUi(this);

    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &EllipseDialog::onAccepted);
}

EllipseDialog::~EllipseDialog()
{
    delete ui;
}

void EllipseDialog::onAccepted()
{
    emit accepted(ui->xspin->value(), ui->yspin->value(), ui->widthspin->value(), ui->heightspin->value());
    this->accept();
}
