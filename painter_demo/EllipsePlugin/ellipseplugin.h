#ifndef ELLIPSEPLUGIN_H
#define ELLIPSEPLUGIN_H

#include <QGenericPlugin>
#include "interface.h"

class EllipsePlugin : public QObject, public InsertInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.PainterDemo.EllipsePlugin" FILE "EllipsePlugin.json")
    Q_INTERFACES(InsertInterface)
public:
    QString name() const;
    QPainterPath getObject(QWidget *parent);
public slots:
    void onDialogAccepted(qreal x, qreal y, qreal wid, qreal hgt);
private:
    qreal m_x;
    qreal m_y;
    qreal width;
    qreal height;
};

#endif // ELLIPSEPLUGIN_H
