#-------------------------------------------------
#
# Project created by QtCreator 2017-05-09T16:38:10
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = EllipsePlugin
TEMPLATE = lib
CONFIG += plugin

DESTDIR = ../plugins

SOURCES += ellipseplugin.cpp \
    ellipsedialog.cpp

HEADERS += ellipseplugin.h \
    ellipsedialog.h
DISTFILES += EllipsePlugin.json

INCLUDEPATH += ../main

unix {
    target.path = /usr/lib
    INSTALLS += target
}

FORMS += \
    ellipsedialog.ui
