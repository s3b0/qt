#include "ellipseplugin.h"
#include "ellipsedialog.h"

QString EllipsePlugin::name() const
{
    return QString("EllipsePlugin");
}

QPainterPath EllipsePlugin::getObject(QWidget *parent)
{
    m_x = 0;
    m_y = 0;
    width = 0;
    height = 0;
    EllipseDialog *dlg = new EllipseDialog(parent);
    connect(dlg, &EllipseDialog::accepted, this, &EllipsePlugin::onDialogAccepted);
    dlg->exec();

    QPainterPath ppath;
    ppath.addEllipse(m_x, m_y, width, height);
    return ppath;
}

void EllipsePlugin::onDialogAccepted(qreal x, qreal y, qreal wid, qreal hgt)
{
    m_x = x;
    m_y = y;
    width = wid;
    height = hgt;
}
