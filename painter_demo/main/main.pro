QT       += core gui widgets

TARGET = painter_demo
TEMPLATE = app


SOURCES  += main.cpp\
            mainwindow.cpp \
            canvas.cpp

HEADERS  += mainwindow.h \
            canvas.h \
            interface.h

FORMS    += mainwindow.ui

LIBS     += -L../plugins -lTextPlugin
