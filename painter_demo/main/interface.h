#ifndef INTERFACE_H
#define INTERFACE_H

#include <QtPlugin>
#include <QPainterPath>

class InsertInterface
{
public:
    virtual ~InsertInterface() {}
    virtual QString name() const = 0;
    virtual QPainterPath getObject(QWidget *parent) = 0;
};

#define InsertInterface_iid "org.qt-project.Qt.PainterDemo.InsertInterface"
Q_DECLARE_INTERFACE(InsertInterface, InsertInterface_iid)

#endif // INTERFACE_H
