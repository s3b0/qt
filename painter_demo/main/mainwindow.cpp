#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPluginLoader>
#include <QDir>
#include "interface.h"

Q_IMPORT_PLUGIN(TextPlugin)

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    loadPlugins();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::loadPlugins()
{
    foreach (QObject *plugin, QPluginLoader::staticInstances()) {
        generatePluginMenu(plugin);
    }
    QString appPath = qApp->applicationDirPath();
    QString msg = "appPath = "+appPath;
    qInfo( msg.toStdString().c_str() );

    QDir pluginsDir = QDir(appPath);
    pluginsDir.cd("../plugins");
    foreach (QString fileName, pluginsDir.entryList(QDir::Files)) {
        msg = "pluginFileName = "+fileName;
        qInfo( msg.toStdString().c_str() );
        QPluginLoader loader(pluginsDir.absoluteFilePath(fileName));
        QObject *plugin = loader.instance();
        if(plugin) {
            generatePluginMenu(plugin);
        }
    }
}

void MainWindow::generatePluginMenu(QObject *plugin)
{
    InsertInterface *interface = qobject_cast<InsertInterface *>(plugin);
    if(interface) {
        QAction *action = new QAction(interface->name(), plugin);
        connect(action, &QAction::triggered, this, &MainWindow::onInsertInterface);
        ui->menuPlugins->addAction(action);
    }
}

void MainWindow::onInsertInterface()
{
    QAction *action = qobject_cast<QAction *>(sender());
    InsertInterface *insertInterfacePlugin = qobject_cast<InsertInterface *>(action->parent());
    const QPainterPath ppath = insertInterfacePlugin->getObject(this);
    if(!ppath.isEmpty())
            ui->widget->insertPainterPath(ppath);
}
