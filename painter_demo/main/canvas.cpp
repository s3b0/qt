#include "canvas.h"
#include <QMouseEvent>
#include <QPainter>
#include <QStyleOption>

Canvas::Canvas(QWidget *parent) : QWidget(parent) {}

void Canvas::insertPainterPath(const QPainterPath &ppath)
{
    QPainter painter(&image);
    painter.drawPath(ppath);
    this->update();
}

void Canvas::updateImage()
{
    qInfo( "updateImage" );
    QPainter painter(&image);
    painter.setPen(QColor(Qt::black));
    painter.setRenderHint(QPainter::Antialiasing);
    painter.drawPolyline(m_points.data(), m_points.count());
    this->update();
}

void Canvas::resizeEvent(QResizeEvent *event)
{
    qInfo( "resizeEvent" );
    QImage image(event->size(), QImage::Format_RGB32);
    image.fill(QColor(Qt::red));
    QPainter painter(&image);
    painter.drawImage(0, 0, this->image);
    this->image = image;
    QWidget::resizeEvent(event);
}

void Canvas::paintEvent(QPaintEvent *event)
{
    qInfo( "paintEvent" );
    QPainter painter(this);
    QStyleOption opt;
    opt.initFrom(this);
    this->style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);
    painter.drawImage(event->rect().topLeft(), image);
}

void Canvas::mousePressEvent(QMouseEvent *event)
{
    this->m_points.clear();
    this->m_points.append(event->localPos());
    //this->update();
    this->updateImage();
}

void Canvas::mouseReleaseEvent(QMouseEvent *event)
{
    this->m_points.append(event->localPos());
    //this->update();
    this->updateImage();
}

void Canvas::mouseMoveEvent(QMouseEvent *event)
{
    this->m_points.append(event->localPos());
    //this->update();
    this->updateImage();
}
