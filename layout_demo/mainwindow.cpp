#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //ui->holaButton->setEnabled(false);

    //connect(this->ui->bonjourButton, &QPushButton::clicked, this->ui->helloButton, &QPushButton::clicked);
    connect(this->ui->bonjourButton, &QPushButton::clicked, [this]() { emit this->ui->helloButton->clicked(); });

    connect(this->ui->helloButton, &QPushButton::clicked, this, &MainWindow::displayHello);
    connect(this->ui->holaButton, &QPushButton::clicked, this, &MainWindow::onHolaClicked);
    this->bonjourConnection = connect(this->ui->bonjourButton, &QPushButton::clicked, [this]() {
        this->ui->plainTextEdit->appendPlainText(QString("Bonjour"));
    });

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::displayHello()
{
    this->ui->plainTextEdit->appendPlainText(QString("Hello"));
}

void MainWindow::onHolaClicked()
{
    this->ui->plainTextEdit->appendPlainText(QString("Hola"));
    disconnect(this->ui->helloButton, &QPushButton::clicked, this, &MainWindow::displayHello);
    disconnect(this->bonjourConnection);
}


