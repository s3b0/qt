#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "processor.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    QObject *firstRootItem = engine.rootObjects().first();
    Processor processor;
    QObject::connect(firstRootItem, SIGNAL(clicked(QString)), &processor, SLOT(onMenuClicked(QString)));

    return app.exec();
}
