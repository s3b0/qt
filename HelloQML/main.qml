import QtQuick 2.5
import QtQuick.Window 2.2

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")
    signal menuClicked(string str)

    MainForm {
        anchors.fill: parent
        mouseArea.onClicked: {
            console.log(qsTr('Clicked on background.'))
            clicked(qsTr('Clicked on background. Text: "' + textEdit.text + '"'))
        }
    }
}
