#include "downloaddialog.h"
#include "ui_downloaddialog.h"
#include <QFileDialog>

DownloadDialog::DownloadDialog(QWidget *parent) : QDialog(parent), ui(new Ui::DownloadDialog)
{
    this->ui->setupUi(this);
    connect(this->ui->buttonBox, &QDialogButtonBox::accepted, this, &DownloadDialog::onButtonAccepted);
    connect(this->ui->pushButton_choose, &QPushButton::clicked, this, &DownloadDialog::onSaveAsButtonClicked);
}

DownloadDialog::~DownloadDialog()
{
    delete ui;
}

void DownloadDialog::onButtonAccepted()
{
    emit accepted(this->ui->lineEdit_url->text(), this->ui->lineEdit_save_as->text());
    this->accept();
}

void DownloadDialog::onSaveAsButtonClicked()
{
    QString path = QFileDialog::getSaveFileName(this, "Save as");
    if(!path.isEmpty())
        this->ui->lineEdit_save_as->setText(path);
}
