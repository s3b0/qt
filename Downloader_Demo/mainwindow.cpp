#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->progressBar->setVisible(false);
    downloader = new Downloader(this);
    connect(ui->pushButton_download, &QPushButton::clicked, this, &MainWindow::onNewDownloadButtonPushed);
    connect(downloader, &Downloader::errorString, this, &MainWindow::showMessage);
    connect(downloader, &Downloader::downloadProgress, this, &MainWindow::onDownloadProgress);
    connect(downloader, &Downloader::available, ui->pushButton_download, &QPushButton::setEnabled);
    connect(downloader, &Downloader::running, ui->progressBar, &QProgressBar::setVisible);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onNewDownloadButtonPushed()
{
    downloaderdialog = new DownloadDialog(this);
    connect(downloaderdialog, &DownloadDialog::accepted, downloader, &Downloader::download);
    downloaderdialog->exec();
    downloaderdialog->deleteLater();
}

void MainWindow::showMessage(const QString &message)
{
    ui->statusBar->showMessage(message, 3000);
}

void MainWindow::onDownloadProgress(qint64 max, qint64 value)
{
    ui->progressBar->setMaximum(max);
    ui->progressBar->setValue(value);
}
