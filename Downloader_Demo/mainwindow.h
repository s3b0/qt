#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "downloader.h"
#include "downloaddialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    DownloadDialog *downloaderdialog;
    Downloader *downloader;
private slots:
    void onNewDownloadButtonPushed();
    void showMessage(const QString &message);
    void onDownloadProgress(qint64 max, qint64 value);
};

#endif // MAINWINDOW_H
