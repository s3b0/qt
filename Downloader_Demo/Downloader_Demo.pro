#-------------------------------------------------
#
# Project created by QtCreator 2017-05-15T10:04:10
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Downloader_Demo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    downloader.cpp \
    downloaddialog.cpp \
    downloadworker.cpp

HEADERS  += mainwindow.h \
    downloader.h \
    downloaddialog.h \
    downloadworker.h

FORMS    += mainwindow.ui \
    downloaddialog.ui
