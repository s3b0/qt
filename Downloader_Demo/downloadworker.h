#ifndef DOWNLOADWORKER_H
#define DOWNLOADWORKER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>

class DownloadWorker : public QObject
{
    Q_OBJECT

signals:
    void downloadProgress(qint64 max, qint64 value);
public slots:
    void doDownload(const QUrl &url, QNetworkAccessManager *nm);
};

#endif // DOWNLOADWORKER_H
