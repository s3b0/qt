#include "downloader.h"
#include <QFile>
#include <QNetworkRequest>
#include <QNetworkReply>

Downloader::Downloader(QObject *parent) : QObject(parent)
{
    this->naManager = new QNetworkAccessManager(this);
    this->worker = new DownloadWorker;
    worker->moveToThread(&workerThread);


    connect(this->naManager, &QNetworkAccessManager::finished, this, &Downloader::onDownloadFinished);
    connect(&workerThread, &QThread::finished, worker, &DownloadWorker::deleteLater);
    connect(worker, &DownloadWorker::downloadProgress, this, &Downloader::downloadProgress);

    workerThread.start();
}

Downloader::~Downloader()
{
    workerThread.quit();
    workerThread.wait();
}

void Downloader::saveToDisk(QNetworkReply *nReply)
{
    QFile file(this->saveFile);
    file.open(QIODevice::WriteOnly | QIODevice::Truncate);
    file.write(nReply->readAll());
    file.close();
}

void Downloader::download(const QUrl &url, const QString &file)
{
    this->saveFile = file;
    worker->doDownload(url, naManager);
    emit this->available(false);
    emit this->running(true);
}

void Downloader::onDownloadFinished(QNetworkReply *nReply)
{
    if(nReply->error() != QNetworkReply::NoError)
        emit this->errorString(nReply->errorString());
    else
        this->saveToDisk(nReply);

    nReply->deleteLater();
    emit this->available(true);
    emit running(false);
}


