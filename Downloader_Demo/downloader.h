#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QObject>
#include <QThread>
#include "downloadworker.h"

class Downloader : public QObject
{
    Q_OBJECT
public:
    explicit Downloader(QObject *parent = 0);
    ~Downloader();
private:
    QNetworkAccessManager *naManager;
    QString saveFile;
    DownloadWorker *worker;
    QThread workerThread;
    void saveToDisk(QNetworkReply *nReply);
signals:
    void errorString(const QString &error);
    void available(bool isAvailable);
    void running(bool isRunning);
    void downloadProgress(qint64, qint64);
public slots:
    void download(const QUrl &url, const QString &file);
private slots:
    void onDownloadFinished(QNetworkReply *nReply);
};

#endif // DOWNLOADER_H
