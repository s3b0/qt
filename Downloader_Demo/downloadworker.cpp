#include "downloadworker.h"

void DownloadWorker::doDownload(const QUrl &url, QNetworkAccessManager *nm)
{
    QNetworkRequest request(url);
    QNetworkReply *response = nm->get(request);
    connect(response, &QNetworkReply::downloadProgress, this, &DownloadWorker::downloadProgress);
}
