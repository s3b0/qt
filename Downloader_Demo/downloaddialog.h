#ifndef DOWNLOADDIALOG_H
#define DOWNLOADDIALOG_H

#include <QDialog>

namespace Ui {
class DownloadDialog;
}

class DownloadDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DownloadDialog(QWidget *parent = 0);
    ~DownloadDialog();

private:
    Ui::DownloadDialog *ui;

signals:
    void accepted(const QUrl&, const QString &);

private slots:
    void onButtonAccepted();
    void onSaveAsButtonClicked();
};

#endif // DOWNLOADDIALOG_H
