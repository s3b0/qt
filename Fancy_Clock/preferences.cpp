#include "preferences.h"
#include "ui_preferences.h"
#include <QSettings>

Preferences::Preferences(QWidget *parent) : QDialog(parent), ui(new Ui::Preferences)
{
    ui->setupUi(this);

    QSettings sts;
    ui->comboBox->setCurrentIndex(sts.value(COLOUR).toInt());
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &Preferences::onAccepted);
}

Preferences::~Preferences()
{
    delete ui;
}

void Preferences::onAccepted()
{
    QSettings sts;
    sts.setValue(COLOUR, ui->comboBox->currentIndex());
}
