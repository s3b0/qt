#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setOrganizationName(QString("S3B0"));
    a.setApplicationName(QString("H3X0R"));
    QWidget wid;
    MainWindow w(&wid);
    w.show();

    return a.exec();
}
