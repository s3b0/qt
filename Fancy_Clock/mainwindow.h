#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QPoint m_mousePos;
    const QString MAIN_GEOMETRY = "MainGeometry";
    const QString MAIN_STATE = "MainState";
    const QString COLOUR = "Colour";
    void setColour();

private slots:
    void updateTime();
    void showContextMenu(const QPoint &pos);
    void showPreferences();

protected:
    void mouseReleaseEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void closeEvent(QCloseEvent *event);

};

#endif // MAINWINDOW_H
