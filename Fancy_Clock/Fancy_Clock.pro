#-------------------------------------------------
#
# Project created by QtCreator 2016-12-16T09:58:34
#
#-------------------------------------------------

QT       += core gui

win32 {
    message("Built on Windows")
}
else: unix: macx{
    message("Built on Mac OS X")
}
else {
    message("Built on Linux")
}

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Fancy_Clock
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    preferences.cpp

HEADERS  += mainwindow.h \
    preferences.h

FORMS    += mainwindow.ui \
    preferences.ui
