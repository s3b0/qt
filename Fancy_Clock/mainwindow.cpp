#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "preferences.h"
#include <QTimer>
#include <QTime>
#include <QMouseEvent>
#include <QPoint>
#include <QMenu>
#include <QAction>
#include <QWidget>
#include <QSettings>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QSettings sts;
    restoreGeometry(sts.value(MAIN_GEOMETRY).toByteArray());
    restoreState(sts.value(MAIN_STATE).toByteArray());

    //setAttribute(Qt::WA_TranslucentBackground);
    setWindowFlags(Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint| Qt::Tool);

    connect(this, &MainWindow::customContextMenuRequested, this, &MainWindow::showContextMenu);

    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &MainWindow::updateTime);
    timer->start(1000);
    this->updateTime();

    setColour();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setColour()
{
    QSettings sts;
    int i = sts.value(COLOUR).toInt();
    QPalette c;
    switch(i) {
        case 0: //black
            c.setColor(QPalette::Foreground, Qt::black);
        break;
        case 1: //white
            c.setColor(QPalette::Foreground, Qt::white);
        break;
        case 2: //green
            c.setColor(QPalette::Foreground, Qt::green);
        break;
        case 3: //red
            c.setColor(QPalette::Foreground, Qt::red);
        break;
    }
    ui->lcdNumber->setPalette(c);
    update();
}

void MainWindow::updateTime()
{
    QTime time = QTime::currentTime();
    QString currentTime = time.toString("hh:mm");
    if(time.second() % 2 == 0)
        currentTime[2] = ' ';
    this->ui->lcdNumber->display(currentTime);
}

void MainWindow::showContextMenu(const QPoint &pos)
{
    QMenu contextMenu;
    contextMenu.addAction(QString("Preferences"), this, SLOT(showPreferences()));
    contextMenu.addAction(QString("Exit"), this, SLOT(close()));
    contextMenu.exec(mapToGlobal(pos));//error
}

void MainWindow::showPreferences()
{
    Preferences *pre = new Preferences(this);
    connect(pre, &Preferences::accepted, this, &MainWindow::setColour);
    pre->exec();
    setColour();
}

void MainWindow::mouseReleaseEvent(QMouseEvent *event)
{
    if(event->button() == Qt::RightButton)
        emit customContextMenuRequested(event->pos());
    else
        QMainWindow::mouseReleaseEvent(event);
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    m_mousePos = event->pos();
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    move(event->globalPos() - m_mousePos);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QSettings sts;
    sts.setValue(MAIN_GEOMETRY, saveGeometry());
    sts.setValue(MAIN_STATE, saveState());
    event->accept();
}
