import QtQuick 2.3
import QtQuick.Controls 1.2
import Base64 1.0

ApplicationWindow {
    visible: true
    width: 180
    height: 100
    title: qsTr("QML Plugin")

    Base64 {
        id: b64
    }

    Column {
        spacing: 6
        anchors {left: parent.left; right: parent.right; top:
        parent.top; bottom: parent.bottom; leftMargin: 6;
        rightMargin: 6; topMargin: 6; bottomMargin: 6}
        Label {
            text: "Input"
        }
        TextField {
            id: input
            width: parent.width
            placeholderText: "Input string here"
            onEditingFinished: bt.text = b64.get(text)
        }
        Label {
            text: "Base64 Encoded"
        }
        TextField {
            id: bt
            readOnly: true
            width: parent.width
        }
    }
}
