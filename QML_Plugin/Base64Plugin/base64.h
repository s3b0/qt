#ifndef BASE64_H
#define BASE64_H
#include <QObject>

class Base64 : public QObject
{
    Q_OBJECT
public:
    explicit Base64(QObject *parent = 0);

public slots:
    QString get(QString in);
};

#endif // BASE64_H
