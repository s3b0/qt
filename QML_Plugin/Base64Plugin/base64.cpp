#include "base64.h"

Base64::Base64(QObject *parent) : QObject(parent) {}

QString Base64::get(QString in)
{
    return QString::fromLocal8Bit(in.toLocal8Bit().toBase64());
}
