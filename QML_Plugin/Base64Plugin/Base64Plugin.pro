#-------------------------------------------------
#
# Project created by QtCreator 2017-05-11T09:00:36
#
#-------------------------------------------------

QT       += core qml

TARGET = Base64Plugin
TEMPLATE = lib
CONFIG += plugin

DESTDIR = ../QML_Plugin/imports/Base64

SOURCES += base64plugin.cpp \
    base64.cpp

HEADERS += base64plugin.h \
    base64.h

OTHER_FILES += qmldir

unix {
    target.path = /usr/lib
    INSTALLS += target
}
