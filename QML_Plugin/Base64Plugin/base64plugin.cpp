#include "base64plugin.h"
#include "base64.h"
#include <QtQml>

void Base64Plugin::registerTypes(const char *uri)
{
    Q_ASSERT(uri == QLatin1String("Base64"));
    qmlRegisterType<Base64>(uri, 1, 0, "Base64");
}
