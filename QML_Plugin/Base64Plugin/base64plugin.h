#ifndef BASE64PLUGIN_H
#define BASE64PLUGIN_H

#include <QQmlExtensionPlugin>

class Base64Plugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QmlExtensionInterface")

public:
    void registerTypes(const char *uri);
};

#endif // BASE64PLUGIN_H
