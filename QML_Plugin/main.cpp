#include <QApplication>
#include <QDir>
#include <QQmlApplicationEngine>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    QDir pluginDir = app.applicationDirPath();
    pluginDir.cd("../QML_Plugin/imports");

    const QString path = pluginDir.absolutePath();
    qInfo( path.toStdString().c_str() );
    engine.addImportPath(path);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
